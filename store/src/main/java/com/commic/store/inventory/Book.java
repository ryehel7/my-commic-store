package com.commic.store.inventory;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity(name = "Book")
@Table(name="book_detail")
public class Book {
    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "author", nullable = false)
    private String author;

    @Column(name = "publisher", nullable = false)
    private String publisher;

    @Column(name = "language", nullable = false)
    private String language;

    @Column(name = "weight", nullable = false)
    private Integer weight;

    @Column(name = "descripton", nullable = false)
    private String descripton;

    @Column(name = "image", nullable = false)
    private String image;

    @ManyToOne(targetEntity = Category.class)
    @JoinColumn(name = "genre_id", referencedColumnName = "id")
    private Category category;


    public Book() {

    }

    public Book(Long id, String title, String author, String publisher, String language, Integer weight, String descripton, String image, Category category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.language = language;
        this.weight = weight;
        this.descripton = descripton;
        this.image = image;
        this.category = category;
    }

    public Book(String title, String author, String publisher, String language, Integer weight, String descripton, String image, Category category) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.language = language;
        this.weight = weight;
        this.descripton = descripton;
        this.image = image;
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", language='" + language + '\'' +
                ", weight=" + weight +
                ", descripton='" + descripton + '\'' +
                ", image='" + image + '\'' +
                ", category=" + category +
                '}';
    }
}
