package com.commic.store.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    public List<Book> getBookDetail() {
        return bookRepository.findAll();
    }

    public void addNewBook(Book book) {
//        to checking title doesntExist
        Optional<Book> bookTitle = bookRepository.findBookByTitle(book.getTitle());
        if(bookTitle.isPresent()){
            throw new IllegalStateException("title already exist");
        }

        bookRepository.save(book);
        System.out.println(book);
    }

    public void deleteBookById(Long bookId) {
        boolean exist = bookRepository.existsById(bookId);
        if(!exist){
            throw new IllegalStateException("Book with id " + bookId + "is not exist");
        }

        bookRepository.deleteById(bookId);
    }
}
