package com.commic.store.inventory;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.List;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> getAllCategory(){
        return categoryRepository.findAll();
    }

    public void addNewBookCategory(Category category) {
        categoryRepository.save(category);
    }

    public  void deleteCategory(Integer categoryId) {
        boolean exist = categoryRepository.existsById(categoryId);

        if(!exist){
            throw new IllegalStateException("Category with id " + categoryId + "doesnt exist");
        }

        categoryRepository.deleteById(categoryId);
    }

}
