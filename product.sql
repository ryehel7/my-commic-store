DROP DATABASE myCommic;

CREATE DATABASE myCommic;

USE myCommic;


CREATE table book_genre( 
	id int(10) not null auto_increment PRIMARY KEY,
	genre varchar(100) not null	UNIQUE
);

INSERT into book_genre(genre)
	VALUES
			('Novel Romance'),
			('Psychology');


CREATE table book_detail( 
	id bigint not null auto_increment PRIMARY KEY ,
	title varchar(100) not null UNIQUE,
	genre_id int(10),
	author varchar(50) not null,
	publisher varchar(50) not null,
	language varchar(20) not null,
	weight int(3),
	descripton varchar(200) not null,
	image varchar(100) not null,
	
	CONSTRAINT `fk_book_genre` FOREIGN KEY (genre_id) REFERENCES book_genre (id)
 );


INSERT into book_detail(title, genre_id, author, publisher, language, weight, descripton, image)
	VALUES 			
			('Kita Kisah yang Terpaksa Usai', 1, 'Dindi Lekstami', 'Transmedia', 'Indonesia', 200, 
			'Teruntuk kita yang sedang merasakan patah hati hebat, percayalah jika semua hanya sementara. Jangan berusaha untuk menghapus luka.',
			'/home'),
			('Rethinking Psychology', 2, 'Jonathan A Smith', 'Nusa Media', 'Indonesia', 400, 
			'RETHINKING PSYCHOLOGY: Dasar-dasar Teoretis dan Konsept Psikologi Baru merupakan terjemahan dari Rethinking Pocbology karya Jonathan A Smith',
			'/home');


		

CREATE table product( 
	id bigint not null auto_increment PRIMARY KEY ,
	book_id bigint not null,
	product_name varchar(100) not null ,
	descripton varchar(200) not null ,
	stock int(3),
	price DECIMAL(10,2),
	
	CONSTRAINT `fk_product_book`  FOREIGN KEY (book_id) REFERENCES book_detail(id)
);

INSERT into product(book_id, product_name, descripton, stock, price)
	VALUES 
		(1, 'Book', 'Kita Kisah yang Terpaksa Usai', 4, 75000),
		(2, 'Book', 'Rethinking Psychology', 4, 75000);


select * from book_genre;
select * from product;

select bk.title, bg.genre , product.price from book_detail bk 
JOIN book_genre bg on bg.id = bk.genre_id
JOIN product on product.book_id = bk.id;

select * from book_detail;

commit;